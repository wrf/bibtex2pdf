#! /usr/bin/env python
#
# v1.0 parses bibtex .bib file and writes metadata to files with pdftk 2014-03-11
# bibtex2pdf.py

from bibtexparser.bparser import BibTexParser
import os
import sys
import subprocess

usage='''
bibtex2pdf.py pdfs_to_update.bib

requires pdftk and python bibtexparser'''

# bibtexparser can be installed in 2.7 using pip as:
# pip install bibtexparser

# pdftk can be installed as:
# apt-get install pdftk

def main(argv, wayout):
	if len(argv) < 2:
		print >> sys.stderr, usage
		sys.exit()

	# use typical -v after the bibtex file to make verbose, for debugging purposes
	verbose = False
	if len(argv) > 2 and argv[2] == '-v':
		verbose = True

	bibfilename = argv[1]
	print >> sys.stderr, "Reading {}".format(bibfilename)
	bibfile = open(bibfilename, 'r')
	parsedbib =  BibTexParser(bibfile)
	bibentries = parsedbib.get_entry_list()
	bibfile.close()

	for e in bibentries:
		# each bibtex entry should have a unique id
		if verbose:
			print >> sys.stderr, e["id"]

		try:
		# bibtex entries list the file as
		# u':home/user/Papers/francis\\_2013\\_opt\\_trans\\_depth.pdf:pdf'
		# so it must split and replace latex backslashes
			targetpdf = "/" + e['file'].split(':')[1].replace('\\','')
		# for cases where there is no file
		except KeyError:
			continue
		outputpdf_base = os.path.basename(targetpdf)
		# as noted in the documenation for os, rename may fail if the 
		# two directories are not in the same file system, thus ~ is used
		outputpdf = os.path.expanduser(os.path.join("~",outputpdf_base))

		if verbose:
			print >> sys.stderr, targetpdf

		pdffields = []
		# if for some reason the field is absent, use a blank
		for i,f in enumerate(['author','title','keyword']):
			try:
				pdffields.append(e[f].encode("ascii","ignore"))
			except KeyError:
				pdffields.append('')

		updatestring = u"InfoKey: Author\nInfoValue: {}\nInfoKey: Title\nInfoValue: {}\nInfoKey: Keyword\nInfoValue: {}\n".format(*pdffields)

		pdftkargs = ["/usr/bin/pdftk", targetpdf, "update_info", "-", "output", outputpdf]
		pdftkcall = subprocess.Popen(pdftkargs, stdin=subprocess.PIPE)
		pdftkcall.communicate(updatestring)

		# if verbose, write the outputpdf name and whether it exists
		if verbose:
			print >> sys.stderr, outputpdf, os.path.exists(outputpdf)

		# check for whether output file exists, if not, skip the step
		# this will catch errors in pdftk, probably due to unicode naming error
		if os.path.exists(outputpdf):
			os.rename(outputpdf, targetpdf)

		if verbose:
			print >> sys.stderr, targetpdf, os.path.exists(targetpdf)
	print >> sys.stderr, "Parsed all files"

if __name__ == "__main__":
	main(sys.argv, sys.stdout)
